*Pick And Place List
*Company=
*Author=
*eMail=
*
*Project=smd filter
*Date=22:45:40
*CreatedBy=Fritzing 0.9.2b.11.19.8d2d5970658f0bed09c661c9ea9a515b5f40f44c
*
*
*Coordinates in mm, always center of component
*Origin 0/0=Lower left corner of PCB
*Rotation in degree (0-360, math. pos.)
*
*No;Value;Package;X;Y;Rotation;Side;Name
1;220;THT;3.61047;-28.0986;0;Bottom;R4
2;1?F;200 mil [THT, tantalum];3.92289;-2.55162;1.19271e-15;Bottom;C91
3;1?F;200 mil [THT, tantalum];3.97171;-13.6796;90;Bottom;R31
4;1?F;200 mil [THT, tantalum];19.6334;-23.0024;90;Bottom;C82
5;;;11.6926;-1.31704;0;Bottom;Via3
6;1?F;200 mil [THT, tantalum];19.1982;-2.75652;1.19271e-15;Bottom;C92
7;1?F;200 mil [THT, tantalum];14.1602;-4.96716;90;Bottom;C12
8;220;THT;8.47062;-16.1164;0;Bottom;R11
9;220;THT;4.21555;-17.9591;0;Bottom;R51
10;1?F;200 mil [THT, tantalum];19.7124;-13.6551;90;Bottom;R32
11;220;THT;19.6627;-28.1127;0;Bottom;R82
12;1?F;200 mil [THT, tantalum];19.3009;-7.69176;1.19271e-15;Bottom;R42
13;;DIP8 [THT];11.7069;-24.2487;0;Bottom;Socket
14;;;10.6129;-10.0565;0;Bottom;Via1
15;1�F;200 mil [THT, electrolytic];17.3891;-34.6733;180;Bottom;C52
16;220;THT;14.8023;-16.1786;180;Bottom;R12
17;1?F;200 mil [THT, tantalum];3.83738;-22.8412;90;Bottom;c81
18;220;THT;19.0954;-17.9545;0;Bottom;R52
19;1?F;200 mil [THT, tantalum];9.37909;-4.96716;90;Bottom;C11
20;;SO 08 [SMD];11.7642;-11.923;90;Top;AOP
21;;;16.3194;-13.8093;0;Bottom;Via2
22;1�F;200 mil [THT, electrolytic];5.80898;-34.686;0;Bottom;C6
23;1?F;200 mil [THT, tantalum];3.80012;-7.68886;1.19271e-15;Bottom;R41
